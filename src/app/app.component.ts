import { Component } from '@angular/core';

@Component({
  selector: 'logs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'logs';
}
