import { Component } from '@angular/core';

@Component({
  selector: 'logs-empty-route',
  template: '',
})
export class EmptyRouteComponent {}
